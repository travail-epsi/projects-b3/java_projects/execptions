package tp1.execptions.calcul;

public class MonException extends Exception{
  public MonException() {
    super("Division par zéro impossible");
  }
}
