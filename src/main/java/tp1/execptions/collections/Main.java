package tp1.execptions.collections;

import java.util.*;

import tp1.execptions.personne.*;

public class Main {
  public static void main(String[] args) {
    List<Personne> c = createList();

    listeDeNombres();

    triListePersonnes(c);

  }

  private static List<Personne> createList() {
    List<Personne> c = new ArrayList<Personne>();

    String s = "Amalvy Theo 170;Roux Michel 181;Clerc Thomas 170;";
    String[] splittedS = s.split(";");

    System.out.println("Create List");
    for (int i = 0; i < splittedS.length; i++) {
      String[] attributs = splittedS[i].split(" ");
      c.add(new Personne(attributs[0].toUpperCase(), attributs[1], Integer.parseInt(attributs[2])));
    }
    c.forEach(p -> {
      System.out.println(p.toString());
    });
    for (Personne p : c) {
      System.out.println(p.toString());
    }
    return c;
  }

  private static void triListePersonnes(List<Personne> c) {
    // trie des personnes
    Collections.sort(c);
    System.out.println("\nAffichage des personnes triés par nom");
    c.forEach(p -> {
      System.out.println(p);
    });

    System.out.println("\nTri par taille");
    /*
     * System.out.println("ecriture classique");
     * Collections.sort(c, new Comparator<Personne>() {
     * 
     * @Override
     * public int compare(Personne o1, Personne o2) {
     * return (o1.getTaille() - o2.getTaille());
     * }
     * });
     */
    System.out.println("ecriture lambda expression");
    Collections.sort(c, (Personne o1, Personne o2) -> (o1.getTaille() - o2.getTaille()));
    c.forEach(p -> {
      System.out.println(p);
    });

    System.out.println("\nTri Par Prénoms");

    Collections.sort(c, (Personne o1, Personne o2) -> (o1.getPrenom().compareTo(o2.getPrenom())));

    c.forEach(p -> {
      System.out.println(p);
    });
  }

  private static void listeDeNombres() {
    System.out.println("\n*******");
    System.out.println("Liste de nombres");
    String[] chiffres = { "un", "deux", "trois", "quatre", "cinq", "six" };
    System.out.println("ordre lexycographique");
    // cinq , deux , quatre , six , trois , un
    List<String> listeS = new ArrayList<>();
    for (String cs : chiffres) {
      listeS.add(cs);
    }
    System.out.println("\nListe de nombres originale");
    // affichage de la liste initiale
    listeS.forEach(lss -> {
      System.out.println(lss);
    });

    Collections.sort(listeS);
    // affichage de la liste triée
    System.out.println("\nListe de nombres Triée");
    listeS.forEach(lss -> {
      System.out.println(lss);
    });
    System.out.println("*******");
  }
}
