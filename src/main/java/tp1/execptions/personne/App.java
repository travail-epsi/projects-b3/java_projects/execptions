package tp1.execptions.personne;

public class App {
    public static void main(String[] args) throws Exception {
      String s = "Amalvy Theo 170;Roux Michel 181;Clerc Thomas 170;";

      String[] splittedS = s.split(";");
      Personne[] persTab = new Personne[splittedS.length];
      fabricTabPersonnes(splittedS, persTab);
  
      for (Personne personne : persTab) {
        System.out.println(personne);
      }
    }

    private static void fabricTabPersonnes(String[] splittedS, Personne[] persTab) {
      for (int i = 0; i < splittedS.length; i++) {
        String[] attributs = splittedS[i].split(" ");
        persTab[i] = new Personne(attributs[0].toUpperCase(), attributs[1], Integer.parseInt(attributs[2]));
      }
    }
}
